// pages/search/search.js
var app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  items:'',
  flag:0,
  tips:''
  },
cancel:function(e){
wx.switchTab({
  url: '../index/index',
})
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    var url = app.globalData.showitem + `?openid=${app.globalData.openid}`;
    wx.request({
      url: url,
      success: function (res) {
        that.setData({
          items: res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  showdetail: function (e) {
    var url = '../detail/detail?name=' + e.currentTarget.id;
    wx.navigateTo({
      url: url,
    })
  },
  input:function(e){
var url=app.globalData.result+`?content=${e.detail.value}`+`&openid=${app.globalData.openid}`;
var that=this;
wx.request({
  url: url,
  success:function(res){
    console.log(res.data);
that.setData({
  tips:res.data,
  flag:1
})
  },
})
  }
})