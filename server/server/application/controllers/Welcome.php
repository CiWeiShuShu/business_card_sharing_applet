<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
  public function recog()
	{
		$this->load->view('recog');
	}
 public function add()
	{
		$this->load->view('add');
	}
public function launch()
	{
		$this->load->view('launch');
	}
 public function showitem()
	{
		$this->load->view('showitem');
	}
 public function detail()
	{
		$this->load->view('detail');
	}
   public function edit()
	{
		$this->load->view('edit');
	}
  public function order()
	{
		$this->load->view('order');
	}
    public function result()
	{
		$this->load->view('result');
	}
}
